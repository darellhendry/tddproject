

function start( jQuery ) {
    $j("#accordion").accordion({
        event:"mouseover"
    });

    $j("#id_theme_button").on("click", function() {
        var body = $j( "#id_body" );
        var button = $j( ".btn" );
        var h1 = $j( "h1" )
        if(body.css("background-color") == "rgb(255, 255, 255)") {
            body.css("font-family", "Roboto Mono");
            body.css("background-color", "rgba(127, 255, 212, 0.60)");
            button.css("background-color", "#007bff");
            h1.css("background-color", "DarkCyan")
        } else {
            body.css("font-family", "Georgia");
            body.css("background-color", "white");
            button.css("background-color", "orange");
            h1.css("background-color", "#5eb1bf")
        }
        alert(body.css("background-color"));
    });
}
var $j = jQuery.noConflict();
$j( document ).ready( start );