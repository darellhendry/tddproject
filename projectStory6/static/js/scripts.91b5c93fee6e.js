

function start( jQuery ) {
    $j("#accordion").accordion({
        event:"mouseover"
    });

    $j("#id_theme_button").on("click", function() {
        var body = $j( "#id_body" );
        if(body.css("background-color") == "rgb(255, 255, 255)") {
            body.css("font-family", "Roboto Mono")
            body.css("background-color", "rgba(127, 255, 212, 0.60)");
        } else {
            body.css("font-family", "Georgia")
            body.css("background-color", "white");
        }
        alert(body.css("background-color"));
    });
}
var $j = jQuery.noConflict();
$j( document ).ready( start );