var favorite = 0;
var nameval = $('#id_name').val();
var emailval = $('#id_email').val();
var passwordval = 'ff';
var valid = false;
var btn = $('#submit_register');
// loading page
function showPage() {
    $("#loader").fadeOut();
    $("#mydiv").fadeIn();
}

// google sign in
// function onSignIn(googleUser) {
//     var profile = googleUser.getBasicProfile();
//     var profileJson = {"profile": {"id": profile.getId(),
//             "name": profile.getName(),
//             "imageUrl": profile.getImageUrl(),
//             "email": profile.getEmail()}};
//
//     localStorage.setItem('profile', JSON.stringify(profileJson));
//     profileJson = JSON.parse(localStorage.getItem('profile'));
//     $('#id_h1_jumbotron').html('Hello ' + profileJson.profile.name + ',<br>Search for your book');
// }



//unsub
function unsubscribe(id) {


    $('#password_confirmation').dialog();
    $('#password_confirmation_button').click(function () {
        var res = $('#password_confirmation_input').val();
        $.ajax({
            method:'POST',
            url:'/id/',
            data:{'id':id, 'password': res}
        }).done(function (data) {
            $('tbody').remove('#tr' + id);
            if (data.response === 'invalid2') {
                alert('wrong password');
            }
            $.get('/check/')
        .done(function (data) {

            var tbody = '';
            var len = data.data.length;
            for (var i = 0; i < len; i++) {
                tbody += '<tr id="tr' +data.data[i].id +'">'+
                    '<td>'+ data.data[i].name  +'</td>'+
                    '<td>'+ data.data[i].email +'</td>'+
                    '<td><button class="btn shadow" onclick=\"unsubscribe('+ data.data[i].id +')\">Unsubscribe</button></td>' +
                    '</tr>'
            }
            $('#id_tbody_user').html(tbody);
        });
            $('#password_confirmation').dialog('close');

        })
    });

}

// favorite feature
function changeStar(id) {
    var img = $('#' + id + ' td img');
    var srcImage = img[1].src;

    if (srcImage.includes('falseStar')) {
        favorite++;
        img[1].src = '../static/css/images/trueStar.png';
    } else {
        favorite--;
        img[1].src = '../static/css/images/falseStar.png';
    }
    $('#id_favorite').html('Favorite : ' + favorite);
}



// Main Function
function start( jQuery ) {
    $('#id_name').val('');
    $('#id_email').val('');

    // Input interaction for button state
    btn.prop('disabled', true);
    btn.css('background-color', 'grey');
    $('.input').change(function () {
        $.ajax({
           method:'POST',
           url:'/check/',
           data:{'email':$('#id_email').val()}
        }).done(function (data) {
            valid = data.response !== 'invalid';
            nameval = $('#id_name').val();
            emailval = $('#id_email').val();
            passwordval = $('#id_password').val();

            if (btn.css('background-color') === 'rgb(128, 128, 128)') {
                btn.prop('disabled', true);
            }
            if (nameval !== '' && emailval !== '' && passwordval !== '' && valid && emailval.includes('@')) {
               btn.css('background-color', '#f2b532');
               btn.prop('disabled', false);
            } else {
               btn.css('background-color', 'grey');
               if (!valid && emailval !== '') {
                   alert('please choose another email, email that you choose was taken by other');
               }
               if (!emailval.includes('@') && emailval !== '') {
                   alert('correct your email address');
               }
            }
        });
    });
    btn.click(function () {
       $.ajax({
           url:'/registration/',
           method:'POST',
           data:{'name':nameval, 'email':emailval, 'password':passwordval}
       }).done(function (data) {
           alert('saved' +
               '\nYour Name is '+nameval+
               '\nYour email is '+emailval);
           $('#id_name').val('');
           $('#id_email').val('');
           $('#id_password').val('');
        });
    });

    // show list subscriber
    $.get('/check/')
        .done(function (data) {
            var tbody = '';
            var len = data.data.length;
            for (var i = 0; i < len; i++) {
                tbody += '<tr id="tr' +data.data[i].id +'">'+
                    '<td>'+ data.data[i].name  +'</td>'+
                    '<td>'+ data.data[i].email +'</td>'+
                    '<td><button class="btn shadow" onclick=\"unsubscribe('+ data.data[i].id +')\">Unsubscribe</button></td>' +
                    '</tr>'
            }
            $('#id_tbody_user').html(tbody);

        });

    // Accordion
    var click = true;
    $("#accordion").accordion({
        heightstyle:"content"
    });

    // Change Theme
    $("#id_theme_button").click( function() {
        var body = $('body');
        var btn = $('.btn');
        var h1 = $('h1');
        var txt = $('textarea');
        if (click) {
            body.animate({'background-color': '#e3e0cf'});
            btn.animate({'background-color': '#8d802c'});
            h1.animate({'background-color':'#424a46'});
            txt.animate({'background-color':'#f4fdf8'});
            click = false;
        } else {
            body.animate({'background-color': '#ced3e4'});
            btn.animate({'background-color': '#f2b532'});
            h1.animate({'background-color':'#252839'});
            txt.animate({'background-color':'#edecfa'});
            click = true;
        }
    });

    // Text Area Effect
    $("textarea").focusin(function() {
        $('html').css("background-color","rgba(0, 0, 0, 0.5)");
        $('#body_parent').css("background-color","rgba(0, 0, 0, 0.5)");
        $(this).css({
            "border-color":"#f2b532",
            "border-style":"solid",
            "border-width":"thick"
        });
    });
    $("textarea").focusout(function() {
        $('html').css("background-color","");
        $('#body_parent').css("background-color","");
        $(this).css({
            "border-color":"",
            "border-style":"",
            "border-width":""
        });
    });

    $("#id_search_button").click(function () {
        var query = $('#id_input_search').val();
        var url = '/req/?q=' + query;
        $.ajax(url)
            .done(function (data) {
                data = data['books'];
                var tbody = '';
                for (var i = 0; i < data.length; i++) {
                    tbody += '<tr id=\"'+ data[i].id +'\">'
                        +'<td><img class=\"img-thumbnail\" src=\"'+ data[i].image +'\"</td>'
                        +'<td>' + data[i].author + '</td>'
                        +'<td>' + data[i].title + '</td>'
                        +'<td><img onclick=\"changeStar(\''+ data[i].id +'\')\" src=\"../static/css/images/falseStar.png\"></td>';
                }
                $('tbody').html(tbody);
                if (tbody === '') {
                    alert('not found');
                }
        });
    });
    $.ajax("/req/?q=quilting")
        .done(function (data) {
            data = data['books'];
            var tbody = '';
            for (var i = 0; i < data.length; i++) {
                tbody += '<tr id=\"'+ data[i].id +'\">'
                    +'<td><img class=\"img-thumbnail\" src=\"'+ data[i].image +'\"</td>'
                    +'<td>' + data[i].author + '</td>'
                    +'<td>' + data[i].title + '</td>'
                    +'<td><img onclick=\"changeStar(\''+ data[i].id +'\')\" src=\"../static/css/images/falseStar.png\"></td>';
            }
            $(tbody).appendTo('#id_table_content');
        });
}


$( document ).ready( start );