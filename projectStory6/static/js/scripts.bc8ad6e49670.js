
function showPage() {
    $("#loader").fadeOut();
    $("#mydiv").fadeIn();
}

function start( jQuery ) {
    var click = true;
    $("#accordion").accordion({
        heightstyle:"content"
    });

    $("#id_theme_button").click( function() {
        var body = $('body');
        var btn = $('.btn');
        var h1 = $('h1');
        var txt = $('textarea');
        if (click) {
            body.animate({'background-color': '#e3e0cf'});
            btn.animate({'background-color': '#8d802c'});
            h1.animate({'background-color':'#424a46'});
            txt.animate({'background-color':'#f4fdf8'});
            click = false;
        } else {
            body.animate({'background-color': '#ced3e4'});
            btn.animate({'background-color': '#f2b532'});
            h1.animate({'background-color':'#252839'});
            txt.animate({'background-color':'#edecfa'});
            click = true;
        }
    });

    $("textarea").focusin(function() {
        $('html').css("background-color","rgba(0, 0, 0, 0.5)");
        $('#body_parent').css("background-color","rgba(0, 0, 0, 0.5)");
        $(this).css({
            "border-color":"#f2b532",
            "border-style":"solid",
            "border-width":"thick"
        });
    });
    $("textarea").focusout(function() {
        $('html').css("background-color","");
        $('#body_parent').css("background-color","");
        $(this).css({
            "border-color":"",
            "border-style":"",
            "border-width":""
        });
    });
    
    $.ajax("https://www.googleapis.com/books/v1/volumes?q=quilting")
        .done(function (data) {
            console.log(data.items[0].volumeInfo.title);
            var tbody = '';
            var imageLink = new Array(data.items.length);
            for (var i = 0; i < data.items.length; i++) {
                tbody += '<tr><td><img class="img-thumbnail" src=\"' + data.items[i].volumeInfo.imageLinks.thumbnail + '\"></td>'
                    +'<td>' + data.items[i].volumeInfo.authors[0] + '</td>'
                    +'<td>' + data.items[i].volumeInfo.title + '</td>'
                    +'<td><button><img src="url(../css/images/falseStar.png)"></button></td>'
            }
            $('#id_table_content').add(tbody).appendTo(document.getElementById('id_table_content'));
        });
}

$( document ).ready( start );