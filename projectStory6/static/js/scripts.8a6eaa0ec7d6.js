var favorite = 0;
function showPage() {
    $("#loader").fadeOut();
    $("#mydiv").fadeIn();
}
   function changeStar(id) {
    var img = $('#' + id + ' td img');
    var srcImage = img[1].src;

    if (srcImage.includes('falseStar')) {
        favorite++;
        img[1].src = '../static/css/images/trueStar.png';
    } else {
        favorite--;
        img[1].src = '../static/css/images/falseStar.png';
    }
    $('#id_favorite').html('Favorite : ' + favorite);

}
function start( jQuery ) {

    var click = true;

    $("#accordion").accordion({
        heightstyle:"content"
    });

    // Change Theme
    $("#id_theme_button").click( function() {
        var body = $('body');
        var btn = $('.btn');
        var h1 = $('h1');
        var txt = $('textarea');
        if (click) {
            body.animate({'background-color': '#e3e0cf'});
            btn.animate({'background-color': '#8d802c'});
            h1.animate({'background-color':'#424a46'});
            txt.animate({'background-color':'#f4fdf8'});
            click = false;
        } else {
            body.animate({'background-color': '#ced3e4'});
            btn.animate({'background-color': '#f2b532'});
            h1.animate({'background-color':'#252839'});
            txt.animate({'background-color':'#edecfa'});
            click = true;
        }
    });

    $("textarea").focusin(function() {
        $('html').css("background-color","rgba(0, 0, 0, 0.5)");
        $('#body_parent').css("background-color","rgba(0, 0, 0, 0.5)");
        $(this).css({
            "border-color":"#f2b532",
            "border-style":"solid",
            "border-width":"thick"
        });
    });
    $("textarea").focusout(function() {
        $('html').css("background-color","");
        $('#body_parent').css("background-color","");
        $(this).css({
            "border-color":"",
            "border-style":"",
            "border-width":""
        });
    });

    $("#id_search_button").click(function () {
        var query = $('#id_input_search').val();
        var url = '/req/?q=' + query;
        $.ajax(url)
            .done(function (data) {
                data = data['books'];
                var tbody = '';
                for (var i = 0; i < data.length; i++) {
                    tbody += '<tr id=\"'+ data[i].id +'\">'
                        +'<td><img class=\"img-thumbnail\" src=\"'+ data[i].image +'\"</td>'
                        +'<td>' + data[i].author + '</td>'
                        +'<td>' + data[i].title + '</td>'
                        +'<td><img onclick=\"changeStar(\''+ data[i].id +'\')\" src=\"../static/css/images/falseStar.png\"></td>';
                }
                $('tbody').html(tbody);
                if (tbody === '') {
                    alert('not found');
                }
        });
    });
    $.ajax("/req/?q=quilting")
        .done(function (data) {
            data = data['books'];
            var tbody = '';
            for (var i = 0; i < data.length; i++) {
                tbody += '<tr id=\"'+ data[i].id +'\">'
                    +'<td><img class=\"img-thumbnail\" src=\"'+ data[i].image +'\"</td>'
                    +'<td>' + data[i].author + '</td>'
                    +'<td>' + data[i].title + '</td>'
                    +'<td><img onclick=\"changeStar(\''+ data[i].id +'\')\" src=\"../static/css/images/falseStar.png\"></td>';
            }
            $(tbody).appendTo('#id_table_content');
        });
}


$( document ).ready( start );