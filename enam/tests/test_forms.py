from django.http import HttpRequest
from django.test import TestCase
from django.utils import timezone

from enam.forms.form_status import StatusForm
from enam.models import Status


class TestForm(TestCase):

    def test_valid_form(self):
        s = Status.objects.create(status='sibuk cuy')
        data = {'status': s.status}
        form = StatusForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        s = Status.objects.create(status='')
        data = {'status': s.status}
        form = StatusForm(data=data)
        self.assertFalse(form.is_valid())

    def test_add_something_to_form(self):
        client = self.client.post('/')
        self.assertEqual(client.status_code, 200)