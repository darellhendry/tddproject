from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys

class StatusTest(LiveServerTestCase):

    def setUp(self):

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(3)
        super(StatusTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusTest, self).tearDown()

    def test_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        status = selenium.find_element_by_id("id_status")
        submit = selenium.find_element_by_id("id_submit")

        status.send_keys('Coba coba')

        submit.send_keys(Keys.RETURN)
        assert "Coba coba" in selenium.page_source

    def test_layout_contain_form_and_table_element(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        element1 = selenium.find_element_by_tag_name('form')
        element2 = selenium.find_element_by_tag_name('table')

    def test_click_see_my_profile(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        button = selenium.find_element_by_id('id_profile_button')
        button.send_keys(Keys.RETURN)

        selenium.get('http://127.0.0.1:8000/profile/')

        assert 'Darell Hendry' in selenium.page_source

    def test_css_h1_color_in_home_page(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        body = selenium.find_element_by_tag_name('h1')
        body.value_of_css_property('background-color:DarkCyan')

    def test_css_img_in_profile_page(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile/')

        img = selenium.find_element_by_id('id_img_profile')

        img.value_of_css_property('width:70%')

    def test_textarea_effect(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        textarea = selenium.find_element_by_tag_name('textarea')
        textarea.click()

        bodycolor = selenium.find_element_by_id('body_parent').value_of_css_property('background-color')
        htmlcolor = selenium.find_element_by_tag_name('html').value_of_css_property('background-color')

        self.assertEqual(bodycolor, "rgba(0, 0, 0, 0.5)")
        self.assertEqual(htmlcolor, "rgba(0, 0, 0, 0.5)")

    def test_click_accordion(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile/')

        accordionHeader = selenium.find_element_by_id('ui-id-3')
        accordionHeader.click()

        self.assertIn('aria-hidden=\"false\"', selenium.page_source, 1)
        self.assertIn('aria-hidden=\"true\"', selenium.page_source, 2)

    def test_change_theme(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        button = selenium.find_element_by_id('id_theme_button')
        button.click()
        time.sleep(2)
        bodycolor = selenium.find_element_by_tag_name('body').get_attribute('style')
        self.assertEqual(bodycolor, 'background-color: rgb(227, 224, 207);')

    def test_show_10_data(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/library/')

        # wait ajax request
        time.sleep(2)
        bookimg = selenium.find_elements_by_tag_name('tr')
        # includes 11 or greater  header
        self.assertGreaterEqual(len(bookimg), 11)