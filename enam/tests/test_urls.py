from django.http import JsonResponse
from django.test import TestCase

# Create your tests here.


class TestUrl(TestCase):
    def test_landingpage_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)



    def test_url_profile_is_exist(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)



    def test_get_api(self):
        response = self.client.get('/req/')
        self.assertEqual(response.status_code, 200)