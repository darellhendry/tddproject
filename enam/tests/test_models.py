from django.test import TestCase
from django.utils import timezone

from enam.models import Status, UserFavoriteBook, Subscriber


class TestModel(TestCase):

    def create_status(self, s='sibuk cuy'):
        return Status.objects.create(status=s)

    def create_subscriber(self):
        return Subscriber.objects.create(name='darell', email='darell@gmail.com', password='abcdefg')

    def create_user_favorite_list(self, idUser='9', booklist='[]'):
        return UserFavoriteBook.objects.create(idUser=idUser, booklist=booklist)

    def test_max_length_status_300_char(self):
        statusObject = self.create_status()
        self.assertLessEqual(len(statusObject.status), 300)

    def test_date_is_now(self):
        statusObject = self.create_status()
        temp =  timezone.now() - statusObject.date
        if temp.days == 0:
            self.assertTrue(True)

    def test_add_status_is_done(self):
        response = self.client.post('/add/', data={'status':'TEST_ADD_STATUS'})
        count = Status.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 302)

    def test_as_json_function_of_Subscriber(self):
        subscriber = self.create_subscriber()
        self.assertEqual(type(subscriber.as_json()), type(dict()))

    def test_booklist_is_exist(self):
        obj = self.create_user_favorite_list()
        self.assertEqual(type(eval(obj.booklist)), type(list()))


