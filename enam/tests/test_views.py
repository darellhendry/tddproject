from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.test import TestCase, RequestFactory
from django.urls import resolve

from enam.models import Status, Subscriber
from enam.views import landingpage, add_status


# Create your tests here.
class TestViews(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
    def test_hello_apa_kabar(self):
        request = HttpRequest()
        response = landingpage(request)
        html_response = response.content.decode('utf8')
        text = 'Hello, Apa kabar?'
        self.assertIn(text, html_response)


    def test_add_status_view(self):
        request = self.factory.post('/',data={'status':'test'})
        response = add_status(request=request)
        self.assertEqual(response.status_code, 302)


    def test_there_is_a_javascript(self):
        response = self.client.get('/')
        page = resolve('/')
        self.assertEqual(page.func, landingpage)
        self.assertContains(response, "<script src=\'/static/js/scripts", 1)

    def test_use_formhtml_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'form.html')

    def test_use_profilehtml_template(self):
        response = self.client.get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_library_html(self):
        response = self.client.get('/library/')
        self.assertTemplateUsed(response, 'library.html')
        self.assertEqual(response.status_code, 200)

    def test_function_json_library_return_json(self):
        response = resolve('/req/')
        typeresponse = self.client.get('/req/')
        self.assertEqual(response.view_name, 'enam.views.jsonlibrary')
        self.assertJSONEqual(str(typeresponse.status_code), '200')

    def test_list_subscriber_view(self):
        response = self.client.get('/listsubscriber/')
        self.assertContains(response, "<th>Name</th>", 1)
        self.assertContains(response, "<th>Email</th>", 1)
        self.assertContains(response, "<th>Unsubscribe</th>", 1)

    def test_subscriber_view_json_return(self):
        Subscriber.objects.create(name='adi', email='adi@mail.com', password='nothing')
        response = self.client.get('/check/')
        self.assertJSONEqual(response.content, {'data': [{'id': 1, 'name': 'adi', 'email': 'adi@mail.com'}]})

    def test_valid_email_by_json_returning(self):
        Subscriber.objects.create(name='adi', email='adi@mail.com', password='nothing')
        # invalid email because , there is existing email on database
        response = self.client.post('/check/', data={'email':'adi@mail.com'})
        self.assertJSONEqual(response.content, {'response':'invalid'})

        # valid email
        response = self.client.post('/check/', data={'email': 'ida@mail.com'})
        self.assertJSONEqual(response.content, {'response': 'ok'})

    def test_delete_subscriber(self):
        Subscriber.objects.create(name='adi', email='adi@mail.com', password='nothing')
        response = self.client.post('/id/', data={'password':'nothing', 'id':1})
        self.assertJSONEqual(response.content, {'response':'ok2'})
