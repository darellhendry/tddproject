from django import forms

class RegisForm(forms.Form):
    name = forms.CharField(label='Name :', max_length=25, required=True,
                           widget=forms.TextInput({'class':'input','placeholder':'  Type your name', 'autocomplete':'off'}))
    email = forms.EmailField(label='Email :', max_length=30, required=True,
                             widget=forms.TextInput({'class': 'input','placeholder': '  Your unique email', 'autocomplete':'off', 'type':'email'}))
    password = forms.CharField(widget=forms.PasswordInput({'class':'input'}),min_length=8, max_length=30, required=True)