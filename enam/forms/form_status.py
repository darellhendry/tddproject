from django import forms

class StatusForm(forms.Form):
    status = forms.CharField(
        label='Status ',
        max_length=300,
        widget=forms.Textarea(attrs={'class':'col-12', 'id':'id_status'}))