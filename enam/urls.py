from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include

from enam import views

urlpatterns = [
    path('addfavorite/',views.addfavorite),
    path('getfavorite/', views.getfavorite),
    path('logout/',views.logout, name='logoutdjango'),
    path('success/', views.success_login),
    path('listsubscriber/', views.listsubscriber, name='listsubscriber'),
    path('id/', views.checkDeleteSubscriber),
    path('check/', views.subscriber),
    path('registration/', views.register, name='register'),
    path('req/', views.jsonlibrary),
    path('library/', views.library, name='library'),
    path('add/', views.add_status, name='addstatus'),
    path('profile/', views.profile, name='profile'),
    path('', views.landingpage, name='landingpage')
]
