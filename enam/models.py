import json
from django.db import models

# Create your models here.

class UserFavoriteBook(models.Model):
    idUser = models.IntegerField(default=0)
    booklist = models.CharField(default='[]', max_length=99999)

class Status(models.Model):
    status = models.CharField(default='', max_length=300)
    date = models.DateTimeField(auto_now_add=True)

class Subscriber(models.Model):
    name = models.CharField(default='', max_length=25)
    email = models.EmailField(default='', max_length=30)
    password = models.CharField(default='', max_length=30)

    def as_json(self):
        return dict(
            id=self.id,
            name=self.name,
            email=self.email,
        )