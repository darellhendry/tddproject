# Generated by Django 2.1.1 on 2018-11-28 09:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('enam', '0011_auto_20181128_1538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userfavoritebook',
            name='booklist',
            field=models.CharField(default='[]', max_length=99999),
        ),
    ]
