# Generated by Django 2.1.1 on 2018-11-12 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('enam', '0003_auto_20181011_0325'),
    ]

    operations = [
        migrations.CreateModel(
            name='Favorite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('favorite', models.IntegerField(default=0)),
            ],
        ),
    ]
