import json

from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt


from enam.forms.form_registration import RegisForm
from enam.forms.form_status import StatusForm
from enam.models import Status, Subscriber, UserFavoriteBook
import requests


# Create your views here.



def landingpage(request):
    if request.method == 'POST':
        form = StatusForm(request.POST or None)
        if form.is_valid():
            add_status(request)
    status = Status.objects.all()
    response = {'form_status':StatusForm(), 'statuss':status}
    return render(request, 'form.html', response)

def add_status(request):
    newStatus = Status(status=request.POST['status'])
    newStatus.save()
    return HttpResponseRedirect('/')

def profile(request):
    return render(request, 'profile.html')

def jsonlibrary(request):
    books = list()
    query = request.GET.get('q','quilting')
    response = requests.get(url='https://www.googleapis.com/books/v1/volumes?q='+query)
    data = response.json()
    try:
        for i in range(10):
            try:
                data['items'][i]['volumeInfo']['authors']
            except KeyError:
                data['items'][i]['volumeInfo']['authors'] = 'not found'
            books.append({
                'id':data['items'][i]['id'],
                'title':data['items'][i]['volumeInfo']['title'],
                'author':data['items'][i]['volumeInfo']['authors'][0],
                'image':data['items'][i]['volumeInfo']['imageLinks']['thumbnail'],
            })
        return JsonResponse({'books':books})
    except KeyError:
        return JsonResponse({'books': ''})


def listsubscriber(request):
    return render(request, 'registerlist.html')

def library(request):

    try:
        favorite = eval(UserFavoriteBook.objects.get(idUser=request.user.id).booklist)
        return render(request, 'library.html', {'favorite': len(favorite)})
    except:
        return render(request, 'library.html', {'favorite': 0})



def flaggedStar(request):
    return JsonResponse({'star':request.session['favorite']})
@csrf_exempt
def register(request):
    if request.method == 'POST':
        newSubscriber = Subscriber(
            name=request.POST['name'],
            email=request.POST['email'],
            password=request.POST['password']
        )
        newSubscriber.save()
        return JsonResponse({'name':newSubscriber.name, 'email':newSubscriber.email})
    form = RegisForm()
    return render(request, 'register.html',{'regis_form':form})

@csrf_exempt
def subscriber(request):
    if request.method == 'GET':
        data = Subscriber.objects.all()
        return JsonResponse({'data':[subscriber.as_json() for subscriber in data]})
    elif request.method == 'POST':

        email = request.POST['email']

        data = Subscriber.objects.all()
        for person in data:
            if person.email == email:
                return JsonResponse({'response':'invalid'})
        return JsonResponse({'response':'ok'})

@csrf_exempt
def checkDeleteSubscriber(request):
    if request.method == 'POST':
        objUser = Subscriber.objects.get(id=request.POST['id'])
        passwordConfirmation = request.POST['password']
        if passwordConfirmation == objUser.password:
            objUser.delete()
            return JsonResponse({'response':'ok2'})
        else:
            return JsonResponse({'response': 'invalid2'})

def success_login(request):
    if request.user.is_authenticated:
        print(request.user.id)

        response = render(request, 'success_login.html')
        try:
            UserFavoriteBook.objects.get(idUser=request.user.id)
        except:
            newUserBookFavoriteList = UserFavoriteBook(idUser=request.user.id)
            newUserBookFavoriteList.save()

        return response
    return render(request, 'success_login.html')
    # favorite is a variable contains list of ID


def logout(request):
    auth.logout(request)
    request.session.flush()
    return HttpResponseRedirect('/success/')

@csrf_exempt
def addfavorite(request):

    if request.method == 'POST' and request.user.is_authenticated:
        # DO SOMETHING
        # inisiasi id star dari buku yang diklik
        # inisiasi objek favoritelist user
        bookid = request.POST['id']
        user = UserFavoriteBook.objects.get(idUser=request.user.id)

        # awal sesi user login
        if 'bookid' not in request.session:
            # inisiasi awal buku yang menjadi favorite

            print(type(eval(user.booklist)))
            request.session['bookid'] = eval(user.booklist)

        # akses bookid yang telah disimpan dalam session
        booklist = request.session['bookid']
        if request.POST['action'] == 'add':
            booklist.append(bookid)
        else:
            booklist.remove(bookid)
        print("====> BOOKLIST")
        print(booklist)
        # update booklist favorite user dengan menconvert ke tipe string menggunakan json dumps
        user.booklist = json.dumps(booklist)
        request.session['bookid'] = booklist
        user.save()
        return JsonResponse({'response':'ok', 'favorite':len(booklist)})
    return JsonResponse({'response': 'invalid'})

def getfavorite(request):
    if request.user.is_authenticated:
        try:
            userFavoriteList = UserFavoriteBook.objects.get(idUser=request.user.id)
            return JsonResponse({'flagstar':eval(userFavoriteList.booklist)})
        except:
            return JsonResponse({'flagstar':'invalid user'})
    else:
        return HttpResponseRedirect('/')