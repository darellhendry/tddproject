from django.contrib import admin

# Register your models here.
from enam.models import Status, Subscriber, UserFavoriteBook

admin.site.register(Status)
admin.site.register(Subscriber)
admin.site.register(UserFavoriteBook)